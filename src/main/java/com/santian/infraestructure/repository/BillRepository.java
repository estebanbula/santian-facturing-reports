package com.santian.infraestructure.repository;

import com.santian.infraestructure.repository.entity.BillEntity;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface BillRepository extends R2dbcRepository<BillEntity, Integer> {

    Flux<BillEntity> findByType(String type);
}
