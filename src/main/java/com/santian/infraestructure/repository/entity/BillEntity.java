package com.santian.infraestructure.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigInteger;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table("bill")
public class BillEntity {

    @Id
    @Column(value = "bill_id")
    private Integer id;
    @Column(value = "client")
    private String client;
    @Column(value = "product")
    private String product;
    @Column(value = "billing_date")
    private String billingDate;
    @Column(value = "quantity")
    private Integer quantity;
    @Column(value = "type")
    private String type;
    @Column(value = "total")
    private BigInteger total;
}
