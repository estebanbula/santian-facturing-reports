package com.santian.infraestructure.handler;

import com.santian.domain.model.Bill;
import com.santian.domain.usecase.BillUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class BillHandler {

    private final BillUseCase billUseCase;

    public Mono<ServerResponse> getReport(ServerRequest serverRequest) {
        return ServerResponse
                .ok()
                .body(billUseCase.retrieveReportByType(serverRequest.pathVariable("type")), Bill.class);
    }
}
