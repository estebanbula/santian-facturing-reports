package com.santian.infraestructure.routes;

import com.santian.infraestructure.handler.BillHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class BillRoute {

    @Bean
    public RouterFunction<ServerResponse> billRoutes(BillHandler billHandler) {
        return nest(path("api/report"),
                route()
                        .GET("{type}", billHandler::getReport).build());
    }
}
