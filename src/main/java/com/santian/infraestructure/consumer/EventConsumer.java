package com.santian.infraestructure.consumer;

import com.santian.domain.usecase.BillUseCase;
import com.santian.infraestructure.configuration.RabbitMqConfiguration;
import org.springframework.stereotype.Component;
import reactor.rabbitmq.Receiver;

@Component
public class EventConsumer {

    private final Receiver receiver;
    private final BillUseCase useCase;

    public EventConsumer(Receiver receiver, BillUseCase useCase) {
        this.receiver = receiver;
        this.useCase = useCase;
        receiver
                .consumeManualAck(RabbitMqConfiguration.QUEUE_EVENTS_GENERAL)
                .subscribe(delivery -> {
                    System.out.println("Received message: " + new String(delivery.getBody()));
                    if (delivery.getBody() != null) {
                        delivery.ack();
                        useCase.saveReportFromEvent(new String(delivery.getBody()));
                    } else {
                        delivery.nack(false);
                    }
                });
    }
}
