package com.santian.domain.model;

import lombok.*;

import java.math.BigInteger;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bill {

    private Integer id;
    private String client;
    private String product;
    private String billingDate;
    private Integer quantity;
    private String type;
    private BigInteger total;
}
