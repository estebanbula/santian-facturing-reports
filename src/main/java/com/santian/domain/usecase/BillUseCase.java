package com.santian.domain.usecase;

import com.google.gson.Gson;
import com.santian.application.mapper.BillMapper;
import com.santian.domain.model.Bill;
import com.santian.infraestructure.repository.BillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class BillUseCase {

    private final BillRepository repository;
    public void saveReportFromEvent(String eventBody) {
        Mono.just(new Gson().fromJson(eventBody, Bill.class))
                        .flatMap(bill -> repository.save(BillMapper.toEntity(bill)))
                .subscribe();
    }

    public Flux<Bill> retrieveReportByType(String type) {
        return repository.findByType(type)
                .flatMap(BillMapper::toModel);
    }
}
