package com.santian.application.mapper;

import com.santian.domain.model.Bill;
import com.santian.infraestructure.repository.entity.BillEntity;
import reactor.core.publisher.Mono;

public class BillMapper {

    private BillMapper() {}

    public static Mono<Bill> toModel(BillEntity entity) {
        return Mono.just(new Bill(entity.getId(),
                entity.getClient(),
                entity.getProduct(),
                entity.getBillingDate(),
                entity.getQuantity(),
                entity.getType(),
                entity.getTotal()));
    }

    public static BillEntity toEntity(Bill model) {
        return new BillEntity(model.getId(),
                model.getClient(),
                model.getProduct(),
                model.getBillingDate(),
                model.getQuantity(),
                model.getType(),
                model.getTotal());
    }
}
